﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Polarcycle
{
    public partial class interval : Form
    {
        ArrayList hrArray = new ArrayList();//arrays decalred
        ArrayList spdArray = new ArrayList();
        ArrayList altArray = new ArrayList();
        ArrayList cadArray = new ArrayList();
        ArrayList pwrArray = new ArrayList();


        List<double> heartRate = new List<double>();
        List<double> speed = new List<double>();
        List<double> cadence = new List<double>();
        List<double> altitude = new List<double>();
        List<double> power = new List<double>();
        IDictionary<string, string> para = new Dictionary<string, string>();
        function fnc = new function();
        public interval(ArrayList hrArray, ArrayList spdArray, ArrayList altArray,ArrayList cadArray, ArrayList pwrArray, IDictionary<string, string> paramList)
        {
            InitializeComponent();
            this.hrArray = hrArray;
            this.spdArray = spdArray;
            this.altArray = altArray;
            this.cadArray = cadArray;
            this.pwrArray = pwrArray;
        }

        private void interval_Load(object sender, EventArgs e)
        {
            double hr, spd, cad, alt, pwr, tothr = 0, totspd = 0, totcad = 0, totalt = 0, totpwr = 0, avgHR, avgSpd, avgCad, avgAlt, avgPwr;
            int count = 1, count2 = 0;
            int tot = 0;
            //for (int i=0; i < pwrArray.Count; i++)
            //{
                
            //    tot +=Convert.ToInt32( pwrArray[i]);
            //}
            //int avg = tot / pwrArray.Count;
            double threshHold = 1.05 * (getAverage(pwrArray) *0.95);
            dataGridViewIntervalList.Rows.Clear();
            dataGridViewIntervalList.Columns.Clear();
            dataGridViewIntervalList.ReadOnly = true;

            dataGridViewIntervalList.Columns.Add("Interval", "Interval");
            dataGridViewIntervalList.Columns.Add("Heart Rate", "Heart Rate(BPM)");
            dataGridViewIntervalList.Columns.Add("Speed", "Speed(0.1 Km/hr or MPH)");
            dataGridViewIntervalList.Columns.Add("Cadence", "Cadence(RPM)");
            dataGridViewIntervalList.Columns.Add("Altitude", "Altitude(M/FT)");
            dataGridViewIntervalList.Columns.Add("Power", "Power(WATTS)");
            dataGridViewIntervalList.Columns.Add("Time", "Time");
            dataGridViewSummary.Rows.Clear();
            dataGridViewSummary.Columns.Clear();
            dataGridViewSummary.ReadOnly = true;
            //
            dataGridViewSummary.Columns.Add("Interval", "Interval");
            dataGridViewSummary.Columns.Add("Heart Rate", "Average Heart Rate(BPM)");
            dataGridViewSummary.Columns.Add("Speed", "Average Speed(0.1 Km/hr or MPH)");
            dataGridViewSummary.Columns.Add("Cadence", "Average Cadence(RPM)");
            dataGridViewSummary.Columns.Add("Altitude", "Average Altitude(M/FT)");
            dataGridViewSummary.Columns.Add("Power", "Average Power(WATTS)");
            for (int i = 0; i < pwrArray.Count; i++)
            {


                if (Convert.ToDouble( pwrArray[i]) > threshHold)
                {

                    dataGridViewIntervalList.Rows.Add(new object[] { "Interval" + count, hrArray[i], spdArray[i], cadArray[i], altArray[i], pwrArray[i] });
            
                    hr =Convert.ToDouble( hrArray[i]);
                    tothr += hr;

                    spd = Convert.ToDouble(spdArray[i]);
                    totspd += spd;

                    cad = Convert.ToDouble(cadArray[i]);
                    totcad += cad;

                    alt = Convert.ToDouble(altArray[i]);
                    totalt += alt;

                    pwr = Convert.ToDouble(pwrArray[i]);
                    totpwr += pwr;

                    count2++;//count number of data inserted in an interval

                }

                else if (i != 0 && Convert.ToDouble(pwrArray[i - 1] )> threshHold && Convert.ToDouble(pwrArray[i]) < threshHold)
                {

                    /*Logic is if previsous power data is higher 
                     * than threshold bust current powerdata is 
                     * less than threshold then it considered 
                     * as end of the interval
                     */

                    avgHR = Math.Round(tothr / (count2), 2);
                    avgSpd = Math.Round(totspd / (count2), 2);
                    avgCad = Math.Round(totcad / (count2), 2);
                    avgAlt = Math.Round(totalt / (count2), 2);
                    avgPwr = Math.Round(totpwr / (count2), 2);

                    dataGridViewSummary.Rows.Add(new object[] { "Interval" + count, avgHR, avgSpd, avgCad, avgAlt, avgPwr });
                    count2 = 0;//reseting data count
                    hr = 0; tothr = 0;//reseting average value for next calculation
                    spd = 0; totspd = 0;
                    cad = 0; totcad = 0;
                    alt = 0; totalt = 0;
                    pwr = 0; totpwr = 0;
                    count++;


                }
            }
        }
        public static double getAverage(ArrayList list)
        {
            double total = 0;
            for (int i = 0; i < list.Count; i++)
            {
                total += Convert.ToDouble(list[i]);
            }
            return total / list.Count;
        }

    }
}

