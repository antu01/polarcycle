﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using ZedGraph;
namespace Polarcycle
{
    public partial class StaticGraph : Form
    {
        public StaticGraph()
        {
            InitializeComponent();
        }

        
        ArrayList hrArray, spdArray, cadArray, altArray, pwrArray;
        IDictionary<string, string> parameters;
        public StaticGraph(ArrayList hrArray, ArrayList spdArray, ArrayList cadArray, ArrayList altArray, ArrayList pwrArray, IDictionary<string, string> parameters)
        {
            InitializeComponent();
            this.hrArray = hrArray;
            this.spdArray = spdArray;
            this.cadArray = cadArray;
            this.altArray = altArray;
            this.pwrArray = pwrArray;
            this.parameters = parameters;
        }
        private void SetSize()
        {
            zedGraphControl1.Location = new Point(10, 10);
            zedGraphControl1.Size = new Size(ClientRectangle.Width - 20,
                                    ClientRectangle.Height - 20);
        }

        ZedGraphControl zgc;
        GraphPane pane;

        LineItem hrLine, speedLine, altitudeLine, cadenceLine, powerLine;

        private void checkBoxHrt_CheckedChanged(object sender, EventArgs e)
        {

            if (checkBoxHrt.Checked)
            {
                hrLine.IsVisible = true;
                zgc.Invalidate();
            }
            else
            {
                hrLine.IsVisible = false;
                zgc.Invalidate();
            }
        }

        private void checkBoxSpeed_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxSpeed.Checked)
            {
                speedLine.IsVisible = true;
                zgc.Invalidate();
            }
            else
            {
                speedLine.IsVisible = false;
                zgc.Invalidate();
            }
        }

        private void checkBoxcas_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxcas.Checked)
            {
                cadenceLine.IsVisible = true;
                zgc.Invalidate();
            }
            else
            {
                cadenceLine.IsVisible = false;
                zgc.Invalidate();
            }
        }

        private void checkBoxAlt_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxAlt.Checked)
            {
                altitudeLine.IsVisible = true;
                zgc.Invalidate();
            }
            else
            {
                altitudeLine.IsVisible = false;
                zgc.Invalidate();
            }
        }

        private void checkBoxPwr_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxPwr.Checked)
            {
                powerLine.IsVisible = true;
                zgc.Invalidate();
            }
            else
            {
                powerLine.IsVisible = false;
                zgc.Invalidate();
            }
        }

        private void btnzoom_Click(object sender, EventArgs e)
        {
          
        }

        private void zedGraphControl1_Load(object sender, EventArgs e)
        {

        }

        PointPairList hrPP = new PointPairList();
        PointPairList speedPP = new PointPairList();
        PointPairList altitudePP = new PointPairList();
        PointPairList cadencePP = new PointPairList();
        PointPairList powerPP = new PointPairList();

       
        

        DateTime start;
        private void GetStart()
        {
           string sDate = parameters["Date"];
            int year = Convert.ToInt16(sDate.Substring(0, 4));
            int month = Convert.ToInt16(sDate.Substring(4, 2));
            int day = Convert.ToInt16(sDate.Substring(6, 2));

            start = new DateTime(year, month, day, 0, 0, 0);
        }


        private void StaticGraph_Load(object sender, EventArgs e)
        {
            zgc = zedGraphControl1;
            pane = zgc.GraphPane;

            pane.Title.Text = "HR Data Graph";

            pane.XAxis.Title.Text = "Time";
            pane.XAxis.Type = AxisType.Date;

            this.GetStart();
            pane.XAxis.Scale.Min = new XDate(start);
            pane.XAxis.Scale.Max = new XDate(start.AddSeconds(hrArray.Count));
            zgc.ScrollMinX = new XDate(start);
            zgc.ScrollMaxX = new XDate(start.AddSeconds(hrArray.Count));

            pane.Y2Axis.IsVisible = true;

            var Y3Axis = pane.AddYAxis("Cadence");
            var Y4Axis = pane.AddYAxis("Altitude");
            var Y5Axis = pane.AddYAxis("Power");

            double x, y1, y2, y3, y4, y5;
            for (int i = 0; i < hrArray.Count; i++)
            {
                x = (double)new XDate(start.AddSeconds(i));
                y1 = (int)hrArray[i];
                y2 = (int)cadArray[i];
                y3 = (int)spdArray[i] / 10;
                y4 = (int)altArray[i];
                y5 = (int)pwrArray[i];

                hrPP.Add(x, y1);
                cadencePP.Add(x, y2);
                speedPP.Add(x, y3);
                altitudePP.Add(x, y4);
                powerPP.Add(x, y5);
            }


            hrLine = pane.AddCurve("HeartRate",
                 hrPP, Color.Blue, SymbolType.None);

            cadenceLine = pane.AddCurve("Cadence",
                 cadencePP, Color.Brown, SymbolType.None);
            cadenceLine.YAxisIndex = Y3Axis;

            speedLine = pane.AddCurve("Speed",
                 speedPP, Color.Green, SymbolType.None);
            speedLine.IsY2Axis = true;

            altitudeLine = pane.AddCurve("Altitude",
                 altitudePP, Color.Red, SymbolType.None);
            altitudeLine.YAxisIndex = Y4Axis;

            powerLine = pane.AddCurve("Power",
                 powerPP, Color.Magenta, SymbolType.None);
            powerLine.YAxisIndex = Y5Axis;

            SetSize();
            zgc.AxisChange();
            zgc.Invalidate();

        }
    }
}
