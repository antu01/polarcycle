﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;

namespace Polarcycle
{
    public partial class filesummary : Form
    {
        public filesummary()
        {
            InitializeComponent();
           
        }
       ArrayList hrArray, spdArray, cadArray, altArray, pwrArray;//array declared

        private void labelVersion_Click(object sender, EventArgs e)
        {

        }

        IDictionary<string, string> parameters;//representing generic collection of parameters


        public filesummary(ArrayList hrArray, ArrayList spdArray, ArrayList cadArray, ArrayList altArray, ArrayList pwrArray, IDictionary<string, string> parameters)
        {
            InitializeComponent();
            this.hrArray = hrArray;
            this.spdArray = spdArray;
            this.cadArray = cadArray;
            this.altArray = altArray;
            this.pwrArray = pwrArray;
            this.parameters = parameters;
        }

      
        private void filesummary_Load(object sender, EventArgs e)//loading parameters
        {
            labelVersion.Text = parameters["Version"];
            labelMonitor.Text = parameters["Monitor"];
            labelSmode.Text = parameters["SMode"];
            labelDate.Text = Calculations.getDate(parameters["Date"]).ToShortDateString();
            labelStartTime.Text = parameters["StartTime"];
            labelLength.Text = parameters["Length"];
            labelInterval.Text = parameters["Interval"];
            labelActiveLimit.Text = parameters["ActiveLimit"];
            labelStartDelay.Text = parameters["StartDelay"];
            labelWeight.Text = parameters["Weight"];

            labeldist.Text = Calculations.GetTotalDistance(spdArray).ToString() + " km";//calling calculation class

            labelavghr.Text = Math.Round(Calculations.getAverage(hrArray), 3).ToString();
            labelmxhr.Text = Calculations.getMaximum(hrArray).ToString();
            labelminhr.Text = Calculations.getMinimum(hrArray).ToString();

            labelmxspd.Text = (Calculations.getMaximum(spdArray) / 10).ToString() + " km/h";
            labelavgspd.Text = Math.Round(Calculations.getAverage(spdArray) / 10, 3).ToString() + " km/h";

            labelmxalt.Text = Calculations.getMaximum(altArray).ToString();
            labelavgalt.Text = Math.Round(Calculations.getAverage(altArray), 3).ToString();

            labelmxpwr.Text = Calculations.getMaximum(pwrArray).ToString();
            labelavgpwr.Text = Math.Round(Calculations.getAverage(pwrArray), 3).ToString();

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }
    }
}
