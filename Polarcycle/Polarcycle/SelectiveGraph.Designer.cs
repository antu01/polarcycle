﻿namespace Polarcycle
{
    partial class SelectiveGraph
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label14 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.zedGraphControl1 = new ZedGraph.ZedGraphControl();
            this.labelMaxAlt = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.labelAvgAltitude = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.labelMaxPower = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.labeAvgPower = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.labelMinHear = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.labelMaxHeart = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblTSS = new System.Windows.Forms.Label();
            this.labelAvgHeart = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.lblIF = new System.Windows.Forms.Label();
            this.labelMaxSpeed = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblNP = new System.Windows.Forms.Label();
            this.labelAvgSpeed = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblFT = new System.Windows.Forms.Label();
            this.labeTotDistance = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.DarkRed;
            this.label14.Font = new System.Drawing.Font("Adobe Fan Heiti Std B", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(511, 9);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(268, 44);
            this.label14.TabIndex = 10;
            this.label14.Text = "Selective Graph";
            // 
            // panel2
            // 
            this.panel2.AutoSize = true;
            this.panel2.Controls.Add(this.zedGraphControl1);
            this.panel2.Location = new System.Drawing.Point(685, 56);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(429, 374);
            this.panel2.TabIndex = 9;
            // 
            // zedGraphControl1
            // 
            this.zedGraphControl1.Location = new System.Drawing.Point(3, 57);
            this.zedGraphControl1.Name = "zedGraphControl1";
            this.zedGraphControl1.ScrollGrace = 0D;
            this.zedGraphControl1.ScrollMaxX = 0D;
            this.zedGraphControl1.ScrollMaxY = 0D;
            this.zedGraphControl1.ScrollMaxY2 = 0D;
            this.zedGraphControl1.ScrollMinX = 0D;
            this.zedGraphControl1.ScrollMinY = 0D;
            this.zedGraphControl1.ScrollMinY2 = 0D;
            this.zedGraphControl1.Size = new System.Drawing.Size(423, 300);
            this.zedGraphControl1.TabIndex = 1;
            this.zedGraphControl1.UseExtendedPrintDialog = true;
            // 
            // labelMaxAlt
            // 
            this.labelMaxAlt.AutoSize = true;
            this.labelMaxAlt.Location = new System.Drawing.Point(190, 447);
            this.labelMaxAlt.Name = "labelMaxAlt";
            this.labelMaxAlt.Size = new System.Drawing.Size(89, 13);
            this.labelMaxAlt.TabIndex = 1;
            this.labelMaxAlt.Text = "Maximum Altitude";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(8, 447);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(128, 16);
            this.label10.TabIndex = 28;
            this.label10.Text = "Maximum Altitude";
            // 
            // labelAvgAltitude
            // 
            this.labelAvgAltitude.AutoSize = true;
            this.labelAvgAltitude.Location = new System.Drawing.Point(193, 411);
            this.labelAvgAltitude.Name = "labelAvgAltitude";
            this.labelAvgAltitude.Size = new System.Drawing.Size(85, 13);
            this.labelAvgAltitude.TabIndex = 27;
            this.labelAvgAltitude.Text = "Average Altitude";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(11, 411);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(123, 16);
            this.label9.TabIndex = 26;
            this.label9.Text = "Average Altitude";
            // 
            // labelMaxPower
            // 
            this.labelMaxPower.AutoSize = true;
            this.labelMaxPower.Location = new System.Drawing.Point(190, 369);
            this.labelMaxPower.Name = "labelMaxPower";
            this.labelMaxPower.Size = new System.Drawing.Size(84, 13);
            this.labelMaxPower.TabIndex = 25;
            this.labelMaxPower.Text = "Maximum Power";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(8, 369);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(119, 16);
            this.label8.TabIndex = 24;
            this.label8.Text = "Maximum Power";
            // 
            // labeAvgPower
            // 
            this.labeAvgPower.AutoSize = true;
            this.labeAvgPower.Location = new System.Drawing.Point(190, 323);
            this.labeAvgPower.Name = "labeAvgPower";
            this.labeAvgPower.Size = new System.Drawing.Size(80, 13);
            this.labeAvgPower.TabIndex = 23;
            this.labeAvgPower.Text = "Average Power";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(8, 323);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(114, 16);
            this.label7.TabIndex = 22;
            this.label7.Text = "Average Power";
            // 
            // labelMinHear
            // 
            this.labelMinHear.AutoSize = true;
            this.labelMinHear.Location = new System.Drawing.Point(193, 274);
            this.labelMinHear.Name = "labelMinHear";
            this.labelMinHear.Size = new System.Drawing.Size(103, 13);
            this.labelMinHear.TabIndex = 21;
            this.labelMinHear.Text = "Minimum Heart Rate";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(11, 274);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(147, 16);
            this.label6.TabIndex = 20;
            this.label6.Text = "Minimum Heart Rate";
            // 
            // labelMaxHeart
            // 
            this.labelMaxHeart.AutoSize = true;
            this.labelMaxHeart.Location = new System.Drawing.Point(190, 215);
            this.labelMaxHeart.Name = "labelMaxHeart";
            this.labelMaxHeart.Size = new System.Drawing.Size(106, 13);
            this.labelMaxHeart.TabIndex = 19;
            this.labelMaxHeart.Text = "Maximum Heart Rate";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(8, 215);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(151, 16);
            this.label5.TabIndex = 18;
            this.label5.Text = "Maximum Heart Rate";
            // 
            // lblTSS
            // 
            this.lblTSS.AutoSize = true;
            this.lblTSS.Location = new System.Drawing.Point(556, 169);
            this.lblTSS.Name = "lblTSS";
            this.lblTSS.Size = new System.Drawing.Size(102, 13);
            this.lblTSS.TabIndex = 17;
            this.lblTSS.Text = "Average Heart Rate";
            // 
            // labelAvgHeart
            // 
            this.labelAvgHeart.AutoSize = true;
            this.labelAvgHeart.Location = new System.Drawing.Point(190, 172);
            this.labelAvgHeart.Name = "labelAvgHeart";
            this.labelAvgHeart.Size = new System.Drawing.Size(102, 13);
            this.labelAvgHeart.TabIndex = 16;
            this.labelAvgHeart.Text = "Average Heart Rate";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(374, 166);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(158, 16);
            this.label19.TabIndex = 15;
            this.label19.Text = "Training Stress Score";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel1.Controls.Add(this.labelMaxAlt);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.labelAvgAltitude);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.labelMaxPower);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.labeAvgPower);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.labelMinHear);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.labelMaxHeart);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.lblTSS);
            this.panel1.Controls.Add(this.labelAvgHeart);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.lblIF);
            this.panel1.Controls.Add(this.labelMaxSpeed);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.lblNP);
            this.panel1.Controls.Add(this.labelAvgSpeed);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.lblFT);
            this.panel1.Controls.Add(this.labeTotDistance);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(16, 56);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(663, 436);
            this.panel1.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(8, 172);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(146, 16);
            this.label4.TabIndex = 14;
            this.label4.Text = "Average Heart Rate";
            // 
            // lblIF
            // 
            this.lblIF.AutoSize = true;
            this.lblIF.Location = new System.Drawing.Point(556, 127);
            this.lblIF.Name = "lblIF";
            this.lblIF.Size = new System.Drawing.Size(85, 13);
            this.lblIF.TabIndex = 13;
            this.lblIF.Text = "Maximum Speed";
            // 
            // labelMaxSpeed
            // 
            this.labelMaxSpeed.AutoSize = true;
            this.labelMaxSpeed.Location = new System.Drawing.Point(190, 130);
            this.labelMaxSpeed.Name = "labelMaxSpeed";
            this.labelMaxSpeed.Size = new System.Drawing.Size(85, 13);
            this.labelMaxSpeed.TabIndex = 12;
            this.labelMaxSpeed.Text = "Maximum Speed";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(374, 124);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(113, 16);
            this.label17.TabIndex = 11;
            this.label17.Text = "Intensity Factor";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(8, 130);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(122, 16);
            this.label3.TabIndex = 10;
            this.label3.Text = "Maximum Speed";
            // 
            // lblNP
            // 
            this.lblNP.AutoSize = true;
            this.lblNP.Location = new System.Drawing.Point(556, 84);
            this.lblNP.Name = "lblNP";
            this.lblNP.Size = new System.Drawing.Size(81, 13);
            this.lblNP.TabIndex = 9;
            this.lblNP.Text = "Average Speed";
            // 
            // labelAvgSpeed
            // 
            this.labelAvgSpeed.AutoSize = true;
            this.labelAvgSpeed.Location = new System.Drawing.Point(190, 87);
            this.labelAvgSpeed.Name = "labelAvgSpeed";
            this.labelAvgSpeed.Size = new System.Drawing.Size(81, 13);
            this.labelAvgSpeed.TabIndex = 8;
            this.labelAvgSpeed.Text = "Average Speed";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(374, 84);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(134, 16);
            this.label15.TabIndex = 7;
            this.label15.Text = "Normalized Power";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 16);
            this.label2.TabIndex = 6;
            this.label2.Text = "Average Speed";
            // 
            // lblFT
            // 
            this.lblFT.AutoSize = true;
            this.lblFT.Location = new System.Drawing.Point(556, 53);
            this.lblFT.Name = "lblFT";
            this.lblFT.Size = new System.Drawing.Size(76, 13);
            this.lblFT.TabIndex = 5;
            this.lblFT.Text = "Total Distance";
            // 
            // labeTotDistance
            // 
            this.labeTotDistance.AutoSize = true;
            this.labeTotDistance.Location = new System.Drawing.Point(190, 56);
            this.labeTotDistance.Name = "labeTotDistance";
            this.labeTotDistance.Size = new System.Drawing.Size(76, 13);
            this.labeTotDistance.TabIndex = 4;
            this.labeTotDistance.Text = "Total Distance";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(390, 14);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(132, 16);
            this.label12.TabIndex = 3;
            this.label12.Text = "Advance Matrices";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Adobe Fan Heiti Std B", 8.25F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(108, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(64, 14);
            this.label11.TabIndex = 2;
            this.label11.Text = "Calculation";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(374, 53);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(153, 16);
            this.label13.TabIndex = 29;
            this.label13.Text = "Functional Threshold";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 16);
            this.label1.TabIndex = 30;
            this.label1.Text = "Total Distance";
            // 
            // SelectiveGraph
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1161, 524);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "SelectiveGraph";
            this.Text = "SelectiveGraph";
            this.Load += new System.EventHandler(this.SelectiveGraph_Load);
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel panel2;
        private ZedGraph.ZedGraphControl zedGraphControl1;
        private System.Windows.Forms.Label labelMaxAlt;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label labelAvgAltitude;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label labelMaxPower;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label labeAvgPower;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label labelMinHear;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label labelMaxHeart;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblTSS;
        private System.Windows.Forms.Label labelAvgHeart;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblIF;
        private System.Windows.Forms.Label labelMaxSpeed;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblNP;
        private System.Windows.Forms.Label labelAvgSpeed;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblFT;
        private System.Windows.Forms.Label labeTotDistance;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label1;
    }
}