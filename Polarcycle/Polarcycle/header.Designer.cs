﻿namespace Polarcycle
{
    partial class Header
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Header));
            this.text = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileSummaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.staticGraphToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.intervalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // text
            // 
            this.text.AutoSize = true;
            this.text.Font = new System.Drawing.Font("Adobe Fan Heiti Std B", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text.Location = new System.Drawing.Point(439, 64);
            this.text.Name = "text";
            this.text.Size = new System.Drawing.Size(166, 30);
            this.text.TabIndex = 6;
            this.text.Text = "text goes here";
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 97);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1005, 487);
            this.panel1.TabIndex = 7;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.pictureBox2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.menuStrip1);
            this.panel2.Location = new System.Drawing.Point(3, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1002, 59);
            this.panel2.TabIndex = 8;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(441, 7);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(64, 44);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 7;
            this.pictureBox2.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.DarkRed;
            this.label1.Font = new System.Drawing.Font("Adobe Fan Heiti Std B", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(500, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(202, 44);
            this.label1.TabIndex = 6;
            this.label1.Text = "Polar Cycle ";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileSummaryToolStripMenuItem,
            this.dataDetailsToolStripMenuItem,
            this.staticGraphToolStripMenuItem,
            this.intervalToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1002, 28);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileSummaryToolStripMenuItem
            // 
            this.fileSummaryToolStripMenuItem.BackColor = System.Drawing.Color.Maroon;
            this.fileSummaryToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fileSummaryToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.fileSummaryToolStripMenuItem.Name = "fileSummaryToolStripMenuItem";
            this.fileSummaryToolStripMenuItem.Size = new System.Drawing.Size(110, 24);
            this.fileSummaryToolStripMenuItem.Text = "File Summary";
            this.fileSummaryToolStripMenuItem.Click += new System.EventHandler(this.fileSummaryToolStripMenuItem_Click);
            // 
            // dataDetailsToolStripMenuItem
            // 
            this.dataDetailsToolStripMenuItem.BackColor = System.Drawing.Color.Maroon;
            this.dataDetailsToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataDetailsToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.dataDetailsToolStripMenuItem.Name = "dataDetailsToolStripMenuItem";
            this.dataDetailsToolStripMenuItem.Size = new System.Drawing.Size(103, 24);
            this.dataDetailsToolStripMenuItem.Text = "Data Details";
            this.dataDetailsToolStripMenuItem.Click += new System.EventHandler(this.dataDetailsToolStripMenuItem_Click);
            // 
            // staticGraphToolStripMenuItem
            // 
            this.staticGraphToolStripMenuItem.BackColor = System.Drawing.Color.Maroon;
            this.staticGraphToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticGraphToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.staticGraphToolStripMenuItem.Name = "staticGraphToolStripMenuItem";
            this.staticGraphToolStripMenuItem.Size = new System.Drawing.Size(102, 24);
            this.staticGraphToolStripMenuItem.Text = "Static Graph";
            this.staticGraphToolStripMenuItem.Click += new System.EventHandler(this.staticGraphToolStripMenuItem_Click);
            // 
            // intervalToolStripMenuItem
            // 
            this.intervalToolStripMenuItem.BackColor = System.Drawing.Color.Maroon;
            this.intervalToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.intervalToolStripMenuItem.ForeColor = System.Drawing.SystemColors.Window;
            this.intervalToolStripMenuItem.Name = "intervalToolStripMenuItem";
            this.intervalToolStripMenuItem.Size = new System.Drawing.Size(70, 24);
            this.intervalToolStripMenuItem.Text = "Interval";
            this.intervalToolStripMenuItem.Click += new System.EventHandler(this.intervalToolStripMenuItem_Click);
            // 
            // Header
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1006, 584);
            this.Controls.Add(this.text);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Header";
            this.Text = "Header";
            this.Load += new System.EventHandler(this.Header_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label text;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileSummaryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dataDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem staticGraphToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.ToolStripMenuItem intervalToolStripMenuItem;
    }
}