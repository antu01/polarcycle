﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Collections;
using System.Text.RegularExpressions;
namespace Polarcycle
{
    public partial class Header : Form
    {
        public Header()
        {
            InitializeComponent();
        }
       
        public static string filename = Dashboard.filename;// filename provided which consists of data




        IDictionary<string, string> parameters = new Dictionary<string, string>();

        ArrayList hrArray = new ArrayList();//arrays decalred
        ArrayList spdArray = new ArrayList();
        ArrayList altArray = new ArrayList();
        ArrayList cadArray = new ArrayList();
        ArrayList pwrArray = new ArrayList();
        static string[] DataSplitter(string line)
        {
            return Regex.Split(line, @"\W+");
        }
        static string[] ParamSplitter(string line)//seperating the parameters by = sign
        {
            return line.Split('=');
        }

        private void FileRead(string filename)
        {
            bool isHrData = false, isParameter = false;
            string line;
            System.IO.StreamReader file =
             new System.IO.StreamReader(filename);
            while ((line = file.ReadLine()) != null)
            {
                if (line != null)
                {
                    if (isParameter == true)
                    {
                        string[] param = ParamSplitter(line);
                        //null values are discarded
                        try
                        {
                            if (param.Length.Equals(2))
                            {
                                //stored in IDictionary
                                parameters.Add(param[0], param[1]);

                            }
                        }
                        catch (IndexOutOfRangeException ex)
                        {
                            Console.WriteLine(ex.ToString());
                        }
                    }

                    if (line.Equals("[Params]"))
                    {
                        isParameter = true;
                    }

                    if (isHrData == true)
                    {
                        string[] w = DataSplitter(line);
                        try
                        {
                            //data stored into respective array list
                            hrArray.Add(Convert.ToInt32(w[0]));
                            spdArray.Add(Convert.ToInt32(w[1]));
                            cadArray.Add(Convert.ToInt32(w[2]));
                            altArray.Add(Convert.ToInt32(w[3]));
                            pwrArray.Add(Convert.ToInt32(w[4]));
                        }
                        catch (FormatException ex)
                        {
                            Console.WriteLine(ex.ToString());
                        }
                    }
                    if (line.Equals("[HRData]"))
                    {
                        isHrData = true;
                    }
                }
            }
            file.Close();
        }

  

        private void fileSummaryToolStripMenuItem_Click(object sender, EventArgs e)//filesummary form links in the panel 
        {
           
            text.Text = "File Summary";
            panel1.Controls.Clear();
            Form filesummary = new filesummary(hrArray, spdArray, cadArray, altArray, pwrArray, parameters);
            filesummary.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            filesummary.Size = panel1.Size;
            filesummary.TopLevel = false;
            panel1.Controls.Add(filesummary);
            filesummary.Show();

        }

        private void dataDetailsToolStripMenuItem_Click(object sender, EventArgs e)//datadetals form links to the panel
        {
            text.Text = "Data Details";
            panel1.Controls.Clear();
            Form datadetails= new Datadetails(hrArray, spdArray, cadArray, altArray, pwrArray, parameters);
            datadetails.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            datadetails.Size = panel1.Size;
            datadetails.TopLevel = false;
            panel1.Controls.Add(datadetails);
            datadetails.Show();

        }

        private void staticGraphToolStripMenuItem_Click(object sender, EventArgs e)//staticgraph form links to the panel
        {

            text.Text = "Static Graph";
            panel1.Controls.Clear();
            Form graph = new StaticGraph(hrArray, spdArray, cadArray, altArray, pwrArray, parameters);
            graph.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            graph.Size = panel1.Size;
            graph.TopLevel = false;
            panel1.Controls.Add(graph);
            graph.Show();
        }

        private void Header_Load(object sender, EventArgs e)
        {
            this.FileRead(filename);
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void intervalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            text.Text = "Interval";
            panel1.Controls.Clear();
            Form interval = new interval(hrArray, spdArray, cadArray, altArray, pwrArray, parameters);
            interval.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
           interval.Size = panel1.Size;
            interval.TopLevel = false;
            panel1.Controls.Add(interval);
            interval.Show();
        }
    }
}
