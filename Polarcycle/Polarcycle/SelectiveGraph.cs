﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;
namespace Polarcycle
{
    public partial class SelectiveGraph : Form
    {
        string datetime;
        DateTime start, duration;
        List<double> heartRate = new List<double>();
        List<double> speed = new List<double>();
        List<double> cadence = new List<double>();
        List<double> altitude = new List<double>();
        List<double> power = new List<double>();
        IDictionary<string, string> parameters = new Dictionary<string, string>();
        function fnc = new function();

        public SelectiveGraph(List<double> hrList, List<double> speedList, List<double> cadenceList, List<double> altitudeList, List<double> powerList, IDictionary<string, string> paramList)
        {
            InitializeComponent();
            this.heartRate = hrList;
            this.speed = speedList;
            this.cadence = cadenceList;
            this.altitude = altitudeList;
            this.power = powerList;
            this.parameters = paramList;
        }
        // coordinate instance for curve
        PointPairList hrPPList = new PointPairList();
        PointPairList cadPPList = new PointPairList();
        PointPairList spdPPList = new PointPairList();
        PointPairList altPPList = new PointPairList();
        PointPairList pwrPPList = new PointPairList();
        LineItem hrCurve, cadCurve, spedCurve, altCurve, pwrCurve;
        public SelectiveGraph()
        {
            InitializeComponent();
        }

        private void SelectiveGraph_Load(object sender, EventArgs e)
        {
            zgc = zedGraphControl1;
            myPane = zgc.GraphPane;
            zgc.IsZoomOnMouseCenter = true;
            zgc.IsEnableHZoom = true;
            zgc.IsEnableVZoom = false;
            CreateGraph();
            labeTotDistance.Text = Math.Round(calculateDistance(speed), 2).ToString() + "Km";
            labelMaxHeart.Text = heartRate.Max().ToString();
            labelMaxSpeed.Text = Math.Round((speed.Max() / 10), 2).ToString() + " km/h";
            labelMaxAlt.Text = altitude.Max().ToString() + " meters";
            labeAvgPower.Text = power.Average().ToString();
            labelMaxPower.Text = power.Max().ToString();
            labelAvgAltitude.Text = altitude.Average().ToString();

            //textBoxMaxCadence.Text = cadence.Max().ToString();
            labelMinHear.Text = heartRate.Min().ToString();
            // textBoxMinSpeed.Text = Math.Round((speed.Min() / 10), 2).ToString() + " km/h";
            // textBoxMinCadence.Text = cadence.Min().ToString();
            labelAvgHeart.Text = Math.Round(heartRate.Average(), 2).ToString();
            labelAvgSpeed.Text = Math.Round((speed.Average() / 10), 2).ToString() + " km/h";
            //  textBoxAvgCadence.Text = Math.Round(cadence.Average(), 2).ToString();
            lblFT.Text = Math.Round((power.Average() * 0.95), 2).ToString();
            lblNP.Text = Math.Round((fnc.calcNP(power)), 2).ToString();
            lblIF.Text = Math.Round((fnc.calcIF(fnc.calcNP(power), fnc.calcFTP(power))), 2).ToString();
            string length = parameters["Length"];
            lblTSS.Text = Math.Round((fnc.calcTSS(fnc.calcNP(power), fnc.calcIF(fnc.calcNP(power), fnc.calcFTP(power)), fnc.calcFTP(power), length)), 2).ToString();


        }
        ZedGraphControl zgc;
        GraphPane myPane;
        double x, y1, y2, y3, y4, y5;
        private void SetSize()
        {
            zedGraphControl1.Location = new Point(10, 10);

            zedGraphControl1.Size = new Size(ClientRectangle.Width - 20,
                                    ClientRectangle.Height - 20);
        }
        public void SetTime()
        {
            DateTime date = Convert.ToDateTime(datetime);
            int year = date.Year;
            int month = date.Month;
            int day = date.Day;


            start = new DateTime(year, month, day, date.Hour, date.Minute, date.Second, date.Millisecond);

            duration = start.AddSeconds(heartRate.Count);
        }
        private void CreateGraph()
        {
            myPane.Title.Text = "HR Static Graph";
            myPane.XAxis.Title.Text = "Time (minutes)";
            myPane.IsAlignGrids = true;
            //yaxis scale for heart rate

            myPane.YAxis.Title.Text = "Heart Rate";
            myPane.YAxis.Color = Color.Red;

            myPane.YAxis.Scale.Max = heartRate.Max() + 10;
            myPane.YAxis.Scale.Min = heartRate.Min();


            // Generate a red curve 
            //  "Heard Rate" in the legend
            hrCurve = myPane.AddCurve("Heart Rate",
            hrPPList, Color.Red, SymbolType.None);
            // hrCurve.YAxisIndex = YAxis;
            // if 


            var Y3Axis = myPane.AddYAxis("Speed");
            myPane.YAxisList[Y3Axis].Color = Color.Green;
            myPane.YAxisList[Y3Axis].Scale.Max = speed.Max() + 10;
            myPane.YAxisList[Y3Axis].Scale.Min = speed.Min() - 1;
            // Generate a Green curve 
            // "Speed" in the legend
            spedCurve = myPane.AddCurve("Speed",
                  spdPPList, Color.Green, SymbolType.None);
            spedCurve.YAxisIndex = Y3Axis;

            //yaxix scale for Cadence
            var Y2Axis = myPane.AddYAxis("Cadence");
            myPane.YAxisList[Y2Axis].Color = Color.Blue;
            myPane.YAxisList[Y2Axis].Scale.Max = cadence.Max() + 10;
            myPane.YAxisList[Y2Axis].Scale.Min = cadence.Min() - 1;
            // Generate a blue curve 
            // "Cadence" in the legend
            cadCurve = myPane.AddCurve("Cadence",
            cadPPList, Color.Blue, SymbolType.None);

            //yaxix scale for Cadence


            //yaxix scale for Altitude
            var Y4Axis = myPane.AddYAxis("Altitude");
            myPane.YAxisList[Y4Axis].Color = Color.Brown;
            myPane.YAxisList[Y4Axis].Scale.Max = altitude.Max() + 10;
            myPane.YAxisList[Y4Axis].Scale.Min = altitude.Min() - 1;

            // Generate a Yellow curve 
            // "Altitude" in the legend
            altCurve = myPane.AddCurve("Altitude",
            altPPList, Color.Brown, SymbolType.None);
            altCurve.YAxisIndex = Y4Axis;


            //yaxix scale for Power
            var Y5Axis = myPane.AddYAxis("Power");
            myPane.YAxisList[Y5Axis].Color = Color.Magenta;
            myPane.YAxisList[Y5Axis].Scale.Max = power.Max() + 10;
            myPane.YAxisList[Y5Axis].Scale.Min = power.Min() - 1;
            // Generate a Yellow curve 
            // "Altitude" in the legend
            pwrCurve = myPane.AddCurve("Power",
                 pwrPPList, Color.Magenta, SymbolType.None);
            pwrCurve.YAxisIndex = Y5Axis;


            //setting x-axis scale 
            myPane.XAxis.Type = AxisType.Date;
            myPane.XAxis.Scale.Format = "HH:mm:ss";
            myPane.XAxis.MinorGrid.IsVisible = true;
            myPane.XAxis.MajorGrid.IsVisible = true;
            SetTime();
            myPane.XAxis.Scale.Min = new XDate(start);
            myPane.XAxis.Scale.Max = new XDate(duration);
            myPane.XAxis.Scale.MinorUnit = DateUnit.Second;
            //  myPane.XAxis.Scale.MajorUnit = DateUnit.Minute;
            zgc.ScrollMinX = new XDate(start);
            zgc.ScrollMaxX = new XDate(duration);

            for (int i = 0; i < heartRate.Count; i++)
            {
                x = (double)new XDate(start.AddSeconds(i));
                y1 = (int)heartRate[i];
                y2 = (int)cadence[i];
                y3 = (int)speed[i];
                y4 = (int)altitude[i];
                y5 = (int)power[i];

                //points added to point pair list
                hrPPList.Add(x, y1);

                spdPPList.Add(x, y3);

                cadPPList.Add(x, y2);

                altPPList.Add(x, y4);

                pwrPPList.Add(x, y5);


                // Tell ZedGraph to refigure the
                // axes since the data have changed
                SetSize();
                zgc.AxisChange();
                //refresh
                zgc.Invalidate();
            }
        }
        private double calculateDistance(List<double> speedList)
        {
            double totalTimeSec = speedList.Count * 5;// 5 is interval from parameters
            double totalTimeHr = totalTimeSec / (3600);
            double avgSpeeed = speedList.Average() / 10;
            double totDistance = avgSpeeed * totalTimeHr;

            return totDistance;


        }
    }
}
