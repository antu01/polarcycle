﻿namespace Polarcycle
{
    partial class filesummary
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.labelVersion = new System.Windows.Forms.Label();
            this.labelSmode = new System.Windows.Forms.Label();
            this.labelStartTime = new System.Windows.Forms.Label();
            this.labelInterval = new System.Windows.Forms.Label();
            this.labelStartDelay = new System.Windows.Forms.Label();
            this.labeldist = new System.Windows.Forms.Label();
            this.labelmxhr = new System.Windows.Forms.Label();
            this.labelavgspd = new System.Windows.Forms.Label();
            this.labelavgalt = new System.Windows.Forms.Label();
            this.labelavgpwr = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.labelMonitor = new System.Windows.Forms.Label();
            this.labelDate = new System.Windows.Forms.Label();
            this.labelLength = new System.Windows.Forms.Label();
            this.labelActiveLimit = new System.Windows.Forms.Label();
            this.labelWeight = new System.Windows.Forms.Label();
            this.labelavghr = new System.Windows.Forms.Label();
            this.labelminhr = new System.Windows.Forms.Label();
            this.labelmxspd = new System.Windows.Forms.Label();
            this.labelmxalt = new System.Windows.Forms.Label();
            this.labelmxpwr = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(-2, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(0, 0);
            this.panel1.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Font = new System.Drawing.Font("Adobe Fan Heiti Std B", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(7, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(30, 0, 2, 0);
            this.label1.MinimumSize = new System.Drawing.Size(188, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(188, 41);
            this.label1.TabIndex = 8;
            this.label1.Text = "VERSION";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Font = new System.Drawing.Font("Adobe Fan Heiti Std B", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(7, 50);
            this.label2.Margin = new System.Windows.Forms.Padding(30, 0, 2, 0);
            this.label2.MinimumSize = new System.Drawing.Size(188, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(188, 41);
            this.label2.TabIndex = 9;
            this.label2.Text = "SMODE";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Font = new System.Drawing.Font("Adobe Fan Heiti Std B", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(7, 91);
            this.label3.Margin = new System.Windows.Forms.Padding(30, 0, 2, 0);
            this.label3.MinimumSize = new System.Drawing.Size(188, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(188, 41);
            this.label3.TabIndex = 10;
            this.label3.Text = "START TIME";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Font = new System.Drawing.Font("Adobe Fan Heiti Std B", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(7, 132);
            this.label4.Margin = new System.Windows.Forms.Padding(30, 0, 2, 0);
            this.label4.MinimumSize = new System.Drawing.Size(188, 41);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(188, 41);
            this.label4.TabIndex = 11;
            this.label4.Text = "INTERVAL";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Font = new System.Drawing.Font("Adobe Fan Heiti Std B", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(7, 173);
            this.label5.Margin = new System.Windows.Forms.Padding(30, 0, 2, 0);
            this.label5.MinimumSize = new System.Drawing.Size(188, 41);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(188, 41);
            this.label5.TabIndex = 12;
            this.label5.Text = "START DELAY";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Font = new System.Drawing.Font("Adobe Fan Heiti Std B", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(7, 214);
            this.label6.Margin = new System.Windows.Forms.Padding(30, 0, 2, 0);
            this.label6.MinimumSize = new System.Drawing.Size(188, 41);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(188, 41);
            this.label6.TabIndex = 13;
            this.label6.Text = "TOTAL DISTANCE";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label7.Font = new System.Drawing.Font("Adobe Fan Heiti Std B", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(7, 255);
            this.label7.Margin = new System.Windows.Forms.Padding(30, 0, 2, 0);
            this.label7.MinimumSize = new System.Drawing.Size(188, 41);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(188, 41);
            this.label7.TabIndex = 14;
            this.label7.Text = "MAX HR";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label8.Font = new System.Drawing.Font("Adobe Fan Heiti Std B", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(7, 296);
            this.label8.Margin = new System.Windows.Forms.Padding(30, 0, 2, 0);
            this.label8.MinimumSize = new System.Drawing.Size(188, 41);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(188, 41);
            this.label8.TabIndex = 15;
            this.label8.Text = "AVERAGE SPEED";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label9.Font = new System.Drawing.Font("Adobe Fan Heiti Std B", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(7, 337);
            this.label9.Margin = new System.Windows.Forms.Padding(30, 0, 2, 0);
            this.label9.MinimumSize = new System.Drawing.Size(188, 41);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(188, 41);
            this.label9.TabIndex = 16;
            this.label9.Text = "AVERAGE ALTITUDE";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label10.Font = new System.Drawing.Font("Adobe Fan Heiti Std B", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(7, 378);
            this.label10.Margin = new System.Windows.Forms.Padding(30, 0, 2, 0);
            this.label10.MinimumSize = new System.Drawing.Size(188, 41);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(188, 41);
            this.label10.TabIndex = 17;
            this.label10.Text = "AVERAGE POWER";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelVersion
            // 
            this.labelVersion.AutoSize = true;
            this.labelVersion.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.labelVersion.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelVersion.Location = new System.Drawing.Point(215, 9);
            this.labelVersion.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelVersion.MinimumSize = new System.Drawing.Size(188, 41);
            this.labelVersion.Name = "labelVersion";
            this.labelVersion.Size = new System.Drawing.Size(188, 41);
            this.labelVersion.TabIndex = 18;
            this.labelVersion.Text = "label2";
            this.labelVersion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelVersion.Click += new System.EventHandler(this.labelVersion_Click);
            // 
            // labelSmode
            // 
            this.labelSmode.AutoSize = true;
            this.labelSmode.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.labelSmode.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelSmode.Location = new System.Drawing.Point(215, 50);
            this.labelSmode.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelSmode.MinimumSize = new System.Drawing.Size(188, 41);
            this.labelSmode.Name = "labelSmode";
            this.labelSmode.Size = new System.Drawing.Size(188, 41);
            this.labelSmode.TabIndex = 19;
            this.labelSmode.Text = "label2";
            this.labelSmode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelStartTime
            // 
            this.labelStartTime.AutoSize = true;
            this.labelStartTime.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.labelStartTime.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelStartTime.Location = new System.Drawing.Point(215, 91);
            this.labelStartTime.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelStartTime.MinimumSize = new System.Drawing.Size(188, 41);
            this.labelStartTime.Name = "labelStartTime";
            this.labelStartTime.Size = new System.Drawing.Size(188, 41);
            this.labelStartTime.TabIndex = 20;
            this.labelStartTime.Text = "label2";
            this.labelStartTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelInterval
            // 
            this.labelInterval.AutoSize = true;
            this.labelInterval.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.labelInterval.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelInterval.Location = new System.Drawing.Point(215, 132);
            this.labelInterval.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelInterval.MinimumSize = new System.Drawing.Size(188, 41);
            this.labelInterval.Name = "labelInterval";
            this.labelInterval.Size = new System.Drawing.Size(188, 41);
            this.labelInterval.TabIndex = 21;
            this.labelInterval.Text = "label2";
            this.labelInterval.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelStartDelay
            // 
            this.labelStartDelay.AutoSize = true;
            this.labelStartDelay.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.labelStartDelay.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelStartDelay.Location = new System.Drawing.Point(215, 173);
            this.labelStartDelay.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelStartDelay.MinimumSize = new System.Drawing.Size(188, 41);
            this.labelStartDelay.Name = "labelStartDelay";
            this.labelStartDelay.Size = new System.Drawing.Size(188, 41);
            this.labelStartDelay.TabIndex = 22;
            this.labelStartDelay.Text = "label2";
            this.labelStartDelay.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labeldist
            // 
            this.labeldist.AutoSize = true;
            this.labeldist.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.labeldist.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labeldist.Location = new System.Drawing.Point(215, 214);
            this.labeldist.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labeldist.MinimumSize = new System.Drawing.Size(188, 41);
            this.labeldist.Name = "labeldist";
            this.labeldist.Size = new System.Drawing.Size(188, 41);
            this.labeldist.TabIndex = 23;
            this.labeldist.Text = "label2";
            this.labeldist.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelmxhr
            // 
            this.labelmxhr.AutoSize = true;
            this.labelmxhr.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.labelmxhr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelmxhr.Location = new System.Drawing.Point(215, 255);
            this.labelmxhr.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelmxhr.MinimumSize = new System.Drawing.Size(188, 41);
            this.labelmxhr.Name = "labelmxhr";
            this.labelmxhr.Size = new System.Drawing.Size(188, 41);
            this.labelmxhr.TabIndex = 24;
            this.labelmxhr.Text = "label2";
            this.labelmxhr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelavgspd
            // 
            this.labelavgspd.AutoSize = true;
            this.labelavgspd.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.labelavgspd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelavgspd.Location = new System.Drawing.Point(215, 296);
            this.labelavgspd.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelavgspd.MinimumSize = new System.Drawing.Size(188, 41);
            this.labelavgspd.Name = "labelavgspd";
            this.labelavgspd.Size = new System.Drawing.Size(188, 41);
            this.labelavgspd.TabIndex = 25;
            this.labelavgspd.Text = "label2";
            this.labelavgspd.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelavgalt
            // 
            this.labelavgalt.AutoSize = true;
            this.labelavgalt.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.labelavgalt.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelavgalt.Location = new System.Drawing.Point(215, 337);
            this.labelavgalt.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelavgalt.MinimumSize = new System.Drawing.Size(188, 41);
            this.labelavgalt.Name = "labelavgalt";
            this.labelavgalt.Size = new System.Drawing.Size(188, 41);
            this.labelavgalt.TabIndex = 26;
            this.labelavgalt.Text = "label2";
            this.labelavgalt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelavgpwr
            // 
            this.labelavgpwr.AutoSize = true;
            this.labelavgpwr.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.labelavgpwr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelavgpwr.Location = new System.Drawing.Point(215, 377);
            this.labelavgpwr.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelavgpwr.MinimumSize = new System.Drawing.Size(188, 41);
            this.labelavgpwr.Name = "labelavgpwr";
            this.labelavgpwr.Size = new System.Drawing.Size(188, 41);
            this.labelavgpwr.TabIndex = 27;
            this.labelavgpwr.Text = "label2";
            this.labelavgpwr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label20.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label20.Font = new System.Drawing.Font("Adobe Fan Heiti Std B", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(594, 10);
            this.label20.Margin = new System.Windows.Forms.Padding(30, 0, 2, 0);
            this.label20.MinimumSize = new System.Drawing.Size(188, 41);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(188, 41);
            this.label20.TabIndex = 28;
            this.label20.Text = "MONITOR";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label21.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label21.Font = new System.Drawing.Font("Adobe Fan Heiti Std B", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(594, 50);
            this.label21.Margin = new System.Windows.Forms.Padding(30, 0, 2, 0);
            this.label21.MinimumSize = new System.Drawing.Size(188, 41);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(188, 41);
            this.label21.TabIndex = 29;
            this.label21.Text = "DATE";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label22.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label22.Font = new System.Drawing.Font("Adobe Fan Heiti Std B", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(594, 377);
            this.label22.Margin = new System.Windows.Forms.Padding(30, 0, 2, 0);
            this.label22.MinimumSize = new System.Drawing.Size(188, 41);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(188, 41);
            this.label22.TabIndex = 30;
            this.label22.Text = "MAX POWER";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label23.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label23.Font = new System.Drawing.Font("Adobe Fan Heiti Std B", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(594, 336);
            this.label23.Margin = new System.Windows.Forms.Padding(30, 0, 2, 0);
            this.label23.MinimumSize = new System.Drawing.Size(188, 41);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(188, 41);
            this.label23.TabIndex = 31;
            this.label23.Text = "MAX ALTITUDE";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label24.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label24.Font = new System.Drawing.Font("Adobe Fan Heiti Std B", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(594, 295);
            this.label24.Margin = new System.Windows.Forms.Padding(30, 0, 2, 0);
            this.label24.MinimumSize = new System.Drawing.Size(188, 41);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(188, 41);
            this.label24.TabIndex = 32;
            this.label24.Text = "MAX SPEED";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label25.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label25.Font = new System.Drawing.Font("Adobe Fan Heiti Std B", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(594, 254);
            this.label25.Margin = new System.Windows.Forms.Padding(30, 0, 2, 0);
            this.label25.MinimumSize = new System.Drawing.Size(188, 41);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(188, 41);
            this.label25.TabIndex = 33;
            this.label25.Text = "MIN HR";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label26.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label26.Font = new System.Drawing.Font("Adobe Fan Heiti Std B", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(594, 213);
            this.label26.Margin = new System.Windows.Forms.Padding(30, 0, 2, 0);
            this.label26.MinimumSize = new System.Drawing.Size(188, 41);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(188, 41);
            this.label26.TabIndex = 34;
            this.label26.Text = "AVERAGE HR";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label27.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label27.Font = new System.Drawing.Font("Adobe Fan Heiti Std B", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(594, 172);
            this.label27.Margin = new System.Windows.Forms.Padding(30, 0, 2, 0);
            this.label27.MinimumSize = new System.Drawing.Size(188, 41);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(188, 41);
            this.label27.TabIndex = 35;
            this.label27.Text = "WEIGHT";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label28.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label28.Font = new System.Drawing.Font("Adobe Fan Heiti Std B", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(594, 131);
            this.label28.Margin = new System.Windows.Forms.Padding(30, 0, 2, 0);
            this.label28.MinimumSize = new System.Drawing.Size(188, 41);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(188, 41);
            this.label28.TabIndex = 36;
            this.label28.Text = "ACTIVE LIMIT";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label29.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label29.Font = new System.Drawing.Font("Adobe Fan Heiti Std B", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(594, 92);
            this.label29.Margin = new System.Windows.Forms.Padding(30, 0, 2, 0);
            this.label29.MinimumSize = new System.Drawing.Size(188, 41);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(188, 41);
            this.label29.TabIndex = 37;
            this.label29.Text = "LENGTH";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelMonitor
            // 
            this.labelMonitor.AutoSize = true;
            this.labelMonitor.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.labelMonitor.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelMonitor.Location = new System.Drawing.Point(800, 10);
            this.labelMonitor.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelMonitor.MinimumSize = new System.Drawing.Size(188, 41);
            this.labelMonitor.Name = "labelMonitor";
            this.labelMonitor.Size = new System.Drawing.Size(188, 41);
            this.labelMonitor.TabIndex = 38;
            this.labelMonitor.Text = "label2";
            this.labelMonitor.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelDate
            // 
            this.labelDate.AutoSize = true;
            this.labelDate.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.labelDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelDate.Location = new System.Drawing.Point(800, 51);
            this.labelDate.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelDate.MinimumSize = new System.Drawing.Size(188, 41);
            this.labelDate.Name = "labelDate";
            this.labelDate.Size = new System.Drawing.Size(188, 41);
            this.labelDate.TabIndex = 39;
            this.labelDate.Text = "label2";
            this.labelDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelLength
            // 
            this.labelLength.AutoSize = true;
            this.labelLength.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.labelLength.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelLength.Location = new System.Drawing.Point(800, 92);
            this.labelLength.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelLength.MinimumSize = new System.Drawing.Size(188, 41);
            this.labelLength.Name = "labelLength";
            this.labelLength.Size = new System.Drawing.Size(188, 41);
            this.labelLength.TabIndex = 40;
            this.labelLength.Text = "label2";
            this.labelLength.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelActiveLimit
            // 
            this.labelActiveLimit.AutoSize = true;
            this.labelActiveLimit.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.labelActiveLimit.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelActiveLimit.Location = new System.Drawing.Point(800, 133);
            this.labelActiveLimit.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelActiveLimit.MinimumSize = new System.Drawing.Size(188, 41);
            this.labelActiveLimit.Name = "labelActiveLimit";
            this.labelActiveLimit.Size = new System.Drawing.Size(188, 41);
            this.labelActiveLimit.TabIndex = 41;
            this.labelActiveLimit.Text = "label2";
            this.labelActiveLimit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelWeight
            // 
            this.labelWeight.AutoSize = true;
            this.labelWeight.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.labelWeight.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelWeight.Location = new System.Drawing.Point(800, 174);
            this.labelWeight.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelWeight.MinimumSize = new System.Drawing.Size(188, 41);
            this.labelWeight.Name = "labelWeight";
            this.labelWeight.Size = new System.Drawing.Size(188, 41);
            this.labelWeight.TabIndex = 42;
            this.labelWeight.Text = "label2";
            this.labelWeight.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelavghr
            // 
            this.labelavghr.AutoSize = true;
            this.labelavghr.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.labelavghr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelavghr.Location = new System.Drawing.Point(800, 215);
            this.labelavghr.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelavghr.MinimumSize = new System.Drawing.Size(188, 41);
            this.labelavghr.Name = "labelavghr";
            this.labelavghr.Size = new System.Drawing.Size(188, 41);
            this.labelavghr.TabIndex = 43;
            this.labelavghr.Text = "label2";
            this.labelavghr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelminhr
            // 
            this.labelminhr.AutoSize = true;
            this.labelminhr.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.labelminhr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelminhr.Location = new System.Drawing.Point(800, 256);
            this.labelminhr.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelminhr.MinimumSize = new System.Drawing.Size(188, 41);
            this.labelminhr.Name = "labelminhr";
            this.labelminhr.Size = new System.Drawing.Size(188, 41);
            this.labelminhr.TabIndex = 44;
            this.labelminhr.Text = "label2";
            this.labelminhr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelmxspd
            // 
            this.labelmxspd.AutoSize = true;
            this.labelmxspd.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.labelmxspd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelmxspd.Location = new System.Drawing.Point(800, 297);
            this.labelmxspd.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelmxspd.MinimumSize = new System.Drawing.Size(188, 41);
            this.labelmxspd.Name = "labelmxspd";
            this.labelmxspd.Size = new System.Drawing.Size(188, 41);
            this.labelmxspd.TabIndex = 45;
            this.labelmxspd.Text = "label2";
            this.labelmxspd.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelmxalt
            // 
            this.labelmxalt.AutoSize = true;
            this.labelmxalt.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.labelmxalt.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelmxalt.Location = new System.Drawing.Point(800, 338);
            this.labelmxalt.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelmxalt.MinimumSize = new System.Drawing.Size(188, 41);
            this.labelmxalt.Name = "labelmxalt";
            this.labelmxalt.Size = new System.Drawing.Size(188, 41);
            this.labelmxalt.TabIndex = 46;
            this.labelmxalt.Text = "label2";
            this.labelmxalt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelmxpwr
            // 
            this.labelmxpwr.AutoSize = true;
            this.labelmxpwr.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.labelmxpwr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelmxpwr.Location = new System.Drawing.Point(800, 379);
            this.labelmxpwr.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelmxpwr.MinimumSize = new System.Drawing.Size(188, 41);
            this.labelmxpwr.Name = "labelmxpwr";
            this.labelmxpwr.Size = new System.Drawing.Size(188, 41);
            this.labelmxpwr.TabIndex = 47;
            this.labelmxpwr.Text = "label2";
            this.labelmxpwr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // filesummary
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1082, 500);
            this.Controls.Add(this.labelmxpwr);
            this.Controls.Add(this.labelmxalt);
            this.Controls.Add(this.labelmxspd);
            this.Controls.Add(this.labelminhr);
            this.Controls.Add(this.labelavghr);
            this.Controls.Add(this.labelWeight);
            this.Controls.Add(this.labelActiveLimit);
            this.Controls.Add(this.labelLength);
            this.Controls.Add(this.labelDate);
            this.Controls.Add(this.labelMonitor);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.labelavgpwr);
            this.Controls.Add(this.labelavgalt);
            this.Controls.Add(this.labelavgspd);
            this.Controls.Add(this.labelmxhr);
            this.Controls.Add(this.labeldist);
            this.Controls.Add(this.labelStartDelay);
            this.Controls.Add(this.labelInterval);
            this.Controls.Add(this.labelStartTime);
            this.Controls.Add(this.labelSmode);
            this.Controls.Add(this.labelVersion);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Name = "filesummary";
            this.Text = "filesummary";
            this.Load += new System.EventHandler(this.filesummary_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label labelVersion;
        private System.Windows.Forms.Label labelSmode;
        private System.Windows.Forms.Label labelStartTime;
        private System.Windows.Forms.Label labelInterval;
        private System.Windows.Forms.Label labelStartDelay;
        private System.Windows.Forms.Label labeldist;
        private System.Windows.Forms.Label labelmxhr;
        private System.Windows.Forms.Label labelavgspd;
        private System.Windows.Forms.Label labelavgalt;
        private System.Windows.Forms.Label labelavgpwr;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label labelMonitor;
        private System.Windows.Forms.Label labelDate;
        private System.Windows.Forms.Label labelLength;
        private System.Windows.Forms.Label labelActiveLimit;
        private System.Windows.Forms.Label labelWeight;
        private System.Windows.Forms.Label labelavghr;
        private System.Windows.Forms.Label labelminhr;
        private System.Windows.Forms.Label labelmxspd;
        private System.Windows.Forms.Label labelmxalt;
        private System.Windows.Forms.Label labelmxpwr;
    }
}