﻿namespace Polarcycle
{
    partial class StaticGraph
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.zedGraphControl1 = new ZedGraph.ZedGraphControl();
            this.checkBoxHrt = new System.Windows.Forms.CheckBox();
            this.checkBoxSpeed = new System.Windows.Forms.CheckBox();
            this.checkBoxcas = new System.Windows.Forms.CheckBox();
            this.checkBoxAlt = new System.Windows.Forms.CheckBox();
            this.checkBoxPwr = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // zedGraphControl1
            // 
            this.zedGraphControl1.Location = new System.Drawing.Point(45, 107);
            this.zedGraphControl1.Name = "zedGraphControl1";
            this.zedGraphControl1.ScrollGrace = 0D;
            this.zedGraphControl1.ScrollMaxX = 0D;
            this.zedGraphControl1.ScrollMaxY = 0D;
            this.zedGraphControl1.ScrollMaxY2 = 0D;
            this.zedGraphControl1.ScrollMinX = 0D;
            this.zedGraphControl1.ScrollMinY = 0D;
            this.zedGraphControl1.ScrollMinY2 = 0D;
            this.zedGraphControl1.Size = new System.Drawing.Size(781, 395);
            this.zedGraphControl1.TabIndex = 2;
            this.zedGraphControl1.UseExtendedPrintDialog = true;
            this.zedGraphControl1.Load += new System.EventHandler(this.zedGraphControl1_Load);
            // 
            // checkBoxHrt
            // 
            this.checkBoxHrt.AutoSize = true;
            this.checkBoxHrt.Location = new System.Drawing.Point(12, 13);
            this.checkBoxHrt.Name = "checkBoxHrt";
            this.checkBoxHrt.Size = new System.Drawing.Size(75, 17);
            this.checkBoxHrt.TabIndex = 3;
            this.checkBoxHrt.Text = "HeartRate";
            this.checkBoxHrt.UseVisualStyleBackColor = true;
            this.checkBoxHrt.CheckedChanged += new System.EventHandler(this.checkBoxHrt_CheckedChanged);
            // 
            // checkBoxSpeed
            // 
            this.checkBoxSpeed.AutoSize = true;
            this.checkBoxSpeed.Location = new System.Drawing.Point(93, 13);
            this.checkBoxSpeed.Name = "checkBoxSpeed";
            this.checkBoxSpeed.Size = new System.Drawing.Size(57, 17);
            this.checkBoxSpeed.TabIndex = 4;
            this.checkBoxSpeed.Text = "Speed";
            this.checkBoxSpeed.UseVisualStyleBackColor = true;
            this.checkBoxSpeed.CheckedChanged += new System.EventHandler(this.checkBoxSpeed_CheckedChanged);
            // 
            // checkBoxcas
            // 
            this.checkBoxcas.AutoSize = true;
            this.checkBoxcas.Location = new System.Drawing.Point(156, 13);
            this.checkBoxcas.Name = "checkBoxcas";
            this.checkBoxcas.Size = new System.Drawing.Size(69, 17);
            this.checkBoxcas.TabIndex = 5;
            this.checkBoxcas.Text = "Cadence";
            this.checkBoxcas.UseVisualStyleBackColor = true;
            this.checkBoxcas.CheckedChanged += new System.EventHandler(this.checkBoxcas_CheckedChanged);
            // 
            // checkBoxAlt
            // 
            this.checkBoxAlt.AutoSize = true;
            this.checkBoxAlt.Location = new System.Drawing.Point(231, 13);
            this.checkBoxAlt.Name = "checkBoxAlt";
            this.checkBoxAlt.Size = new System.Drawing.Size(61, 17);
            this.checkBoxAlt.TabIndex = 6;
            this.checkBoxAlt.Text = "Altitude";
            this.checkBoxAlt.UseVisualStyleBackColor = true;
            this.checkBoxAlt.CheckedChanged += new System.EventHandler(this.checkBoxAlt_CheckedChanged);
            // 
            // checkBoxPwr
            // 
            this.checkBoxPwr.AutoSize = true;
            this.checkBoxPwr.Location = new System.Drawing.Point(298, 13);
            this.checkBoxPwr.Name = "checkBoxPwr";
            this.checkBoxPwr.Size = new System.Drawing.Size(56, 17);
            this.checkBoxPwr.TabIndex = 7;
            this.checkBoxPwr.Text = "Power";
            this.checkBoxPwr.UseVisualStyleBackColor = true;
            this.checkBoxPwr.CheckedChanged += new System.EventHandler(this.checkBoxPwr_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.checkBoxHrt);
            this.panel1.Controls.Add(this.checkBoxSpeed);
            this.panel1.Controls.Add(this.checkBoxPwr);
            this.panel1.Controls.Add(this.checkBoxcas);
            this.panel1.Controls.Add(this.checkBoxAlt);
            this.panel1.Location = new System.Drawing.Point(0, -1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(366, 37);
            this.panel1.TabIndex = 9;
            // 
            // StaticGraph
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(852, 514);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.zedGraphControl1);
            this.Name = "StaticGraph";
            this.Text = "StaticGraph";
            this.Load += new System.EventHandler(this.StaticGraph_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private ZedGraph.ZedGraphControl zedGraphControl1;
        private System.Windows.Forms.CheckBox checkBoxHrt;
        private System.Windows.Forms.CheckBox checkBoxSpeed;
        private System.Windows.Forms.CheckBox checkBoxcas;
        private System.Windows.Forms.CheckBox checkBoxAlt;
        private System.Windows.Forms.CheckBox checkBoxPwr;
        private System.Windows.Forms.Panel panel1;
    }
}