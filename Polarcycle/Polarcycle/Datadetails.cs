﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace Polarcycle
{
    public partial class Datadetails : Form
    {
        int count1, count2;
        public Datadetails()
        {
            InitializeComponent();
        }

       

        ArrayList hrArray, spdArray, cadArray, altArray, pwrArray;

        private void dataGridView1_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            count1 = 0;
            count1 = e.RowIndex;
        }

        private void dataGridView1_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            count2 = 0;
            count2 = e.RowIndex;
            if (count2 - count1 > 2)
            {
                List<double> hrListSeletive = new List<double>();//for selective graph
                List<double> speedListSeletive = new List<double>();//for selective graph
                List<double> cadenceListSeletive = new List<double>();//for selective graph
                List<double> altitudeListSeletive = new List<double>();//for selective graph
                List<double> powerListSeletive = new List<double>();//for selective graph
                for (int i = count1; i <= count2; i++)
                {
                    DataGridViewRow row = dataGridView1.Rows[i];
                    hrListSeletive.Add(Convert.ToDouble(row.Cells[0].Value.ToString()));// adding data to hr list
                    speedListSeletive.Add(Convert.ToDouble(row.Cells[1].Value.ToString()));// adding data to spd list
                    cadenceListSeletive.Add(Convert.ToDouble(row.Cells[2].Value.ToString()));//adding data to cad list
                    altitudeListSeletive.Add(Convert.ToDouble(row.Cells[3].Value.ToString()));//adding data to alt list
                    powerListSeletive.Add(Convert.ToDouble(row.Cells[4].Value.ToString()));//adding data to power list

                }
                Form selGrph = new SelectiveGraph(hrListSeletive, speedListSeletive, cadenceListSeletive, altitudeListSeletive, powerListSeletive, parameters);
                selGrph.Show();

            }
        }

        private void dataGridView1_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }

        IDictionary<string, string> parameters;
        public Datadetails(ArrayList hrArray, ArrayList spdArray, ArrayList cadArray, ArrayList altArray, ArrayList pwrArray, IDictionary<string, string> parameters)
        {
            InitializeComponent();
            this.hrArray = hrArray;
            this.spdArray = spdArray;
            this.cadArray = cadArray;
            this.altArray = altArray;
            this.pwrArray = pwrArray;
            this.parameters = parameters;
        }
        private void Datadetails_Load_1(object sender, EventArgs e)
        {
            dataGridView1.ReadOnly = true;
            dataGridView1.Columns.Add("Heart Rate", "Heart Rate");
            dataGridView1.Columns.Add("Speed", "Speed");
            dataGridView1.Columns.Add("Cadence", "Cadence");
            dataGridView1.Columns.Add("Altitude", "Altitude");
            dataGridView1.Columns.Add("Power", "Power");
            dataGridView1.Columns.Add("Time", "Time");

            for (int i = 0; i < hrArray.Count; i++)
            {
                dataGridView1.Rows.Add(new object[] { hrArray[i], spdArray[i], cadArray[i], altArray[i], pwrArray[i], Calculations.getTime(parameters["Date"], parameters["StartTime"]).AddSeconds(i).ToLongTimeString() });
            }
        }
    }
}

