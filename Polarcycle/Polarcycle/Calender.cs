﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Polarcycle
{
    public partial class Calender : Form
    {
        public static string filename;
        string FileDate = "", startTime = " ", Length = "";
        Dictionary<string, string[]> paraList = new Dictionary<string, string[]>();

       

        public Calender()
        {
            InitializeComponent();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void listBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            string key = listBox1.SelectedItem.ToString();
            foreach (KeyValuePair<string,string[]>abc in paraList)
            {
                if(abc.Value[0]==key.Split('-')[0]&& abc.Value[1]==key.Split('-')[1]&& abc.Value[2]==key.Split('-')[2])
                {
                   
                    filename = abc.Key;
                   Header frm = new Header();
                   frm.Show();
                   
                }
            }
                                                                                                                              
        }

        private void Calender_Load(object sender, EventArgs e)
        {
            string appPath = Path.GetDirectoryName(Application.ExecutablePath) + @"\data\";
            foreach (string fpathName in Directory.GetFiles(appPath, "*.hrm"))//reading all files from data folder
            {
                bool isParameter = false;
                var Lines = File.ReadLines(fpathName);//reading all the lines from the file
                foreach (var line in Lines)
                {
                    if (!string.IsNullOrWhiteSpace(line))
                    {
                        if (isParameter)//checking the data is parameter or not
                        {
                            string[] p = line.Split('=');//splliting data in case of parameter

                            if (p.Length.Equals(2))
                            {
                                try
                                {
                                    if (p[0] == "Date")
                                    {
                                        FileDate = p[1];

                                    }
                                    else if (p[0] == "StartTime")
                                    {
                                        startTime = p[1];

                                    }
                                    else if (p[0] == "Length")
                                    {
                                        Length = p[1];
                                    }
                                }
                                //stores split data in dictionary
                                catch (IndexOutOfRangeException ex)
                                {
                                    Console.WriteLine(ex.ToString());
                                }
                                if (FileDate != "" && startTime != "" && Length != "")
                                {
                                    paraList.Add(fpathName, new string[] { (FileDate), startTime, Length });
                                    FileDate = ""; startTime = ""; Length = "";
                                }

                            }
                        }
                        if(line.Equals("[Params]",StringComparison.OrdinalIgnoreCase))
                        {
                            isParameter = true;

                        }
                      
                    }

                }
            }
            foreach (KeyValuePair<string, string[]> abc in paraList)
            {
                listBox1.Items.Add(abc.Value[0]+"-"+abc.Value[1]+"-"+abc.Value[2]);
            }

        }
        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
            listBox1.Items.Clear();
            DateTime selDateItem = monthCalendar1.SelectionRange.Start;
            string seldate = selDateItem.ToString();
            foreach (KeyValuePair<string, string[]> abc in paraList)
            {
                if (seldate == abc.Value[0])
                {
                    listBox1.Items.Add(abc.Value[0] + "-" + abc.Value[1] + "-" + abc.Value[2]);
                }

            }
        }
    }
    }






