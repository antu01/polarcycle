﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Polarcycle
{
    public partial class Dashboard : Form
    {
        public static string filename;
        public Dashboard()
        {
            InitializeComponent();
        }
        Dictionary<string, string[]> paraList = new Dictionary<string, string[]>();
        string FileDate = "", startTime = " ", Length = "";

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
            listBox1.Items.Clear();
            DateTime selDateItem = monthCalendar1.SelectionRange.Start;
            string seldate = selDateItem.ToString();
            foreach (KeyValuePair<string, string[]> kvp in paraList)
            {
                if (seldate == kvp.Value[0])
                {
                    listBox1.Items.Add(kvp.Value[0] + "#" + kvp.Value[1] + "#" + kvp.Value[2]);
                }

            }
        }
        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }


        private void listBox1_SelectedValueChanged_1(object sender, EventArgs e)
        {

            string key = listBox1.SelectedItem.ToString();
            foreach (KeyValuePair<string, string[]> kvp in paraList)
            {
                if (kvp.Value[0] == key.Split('#')[0] && kvp.Value[1] == key.Split('#')[1] && kvp.Value[2] == key.Split('#')[2])
                {

                    filename = kvp.Key;
                    Form Header = new Header();
                    Header.Show();
                }
            }
        }


        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

      
        private void Dashboard_Load(object sender, EventArgs e)
        {


            string appPath = Path.GetDirectoryName(Application.ExecutablePath) + @"\data\";
            foreach (string fpathName in Directory.GetFiles(appPath, "*.hrm"))//reading all files from data folder
            {
                bool isParameter = false;
                var Lines = File.ReadLines(fpathName);//reading all the lines from the file
                foreach (var line in Lines)
                {
                    if (!string.IsNullOrWhiteSpace(line))
                    {
                        if (isParameter)//checking the data is parameter or not
                        {
                            string[] p = line.Split('=');//splliting data in case of parameter

                            if (p.Length.Equals(2))
                            {
                                try
                                {
                                    if (p[0] == "Date")
                                    {
                                        FileDate = p[1];

                                    }
                                    else if (p[0] == "StartTime")
                                    {
                                        startTime = p[1];

                                    }
                                    else if (p[0] == "Length")
                                    {
                                        Length = p[1];
                                    }
                                }
                                //stores split data in dictionary
                                catch (IndexOutOfRangeException ex)
                                {
                                    Console.WriteLine(ex.ToString());
                                }
                                if (FileDate != "" && startTime != "" && Length != "")
                                {
                                    paraList.Add(fpathName, new string[] { GetDate(FileDate), startTime, Length });
                                    FileDate = ""; startTime = ""; Length = "";
                                }

                            }
                        }
                        if (line.Equals("[Params]", StringComparison.OrdinalIgnoreCase))
                        {
                            isParameter = true;

                        }
                        else if (isParameter && line.Substring(0, 1).Equals("["))
                        {
                            isParameter = false;
                        }

                    }
                }

            }
        }
        public static string GetDate(string date)
        {
            int year = Convert.ToInt32(date.Substring(0, 4));
            int month = Convert.ToInt32(date.Substring(4, 2));
            int day = Convert.ToInt32(date.Substring(6, 2));


            DateTime start = new DateTime(year, month, day);
            string ret = start.ToString();
            return ret;
        }
    }
    }


