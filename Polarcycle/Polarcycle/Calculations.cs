﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;


namespace Polarcycle
{
    class Calculations
    {
        public static double mn = 200;//minimum
        public static double mx = 600;//maximum

        public static DateTime getDate(string date)
        {
            return new DateTime(Convert.ToInt16(date.Substring(0, 4)), Convert.ToInt16(date.Substring(4, 2)), Convert.ToInt16(date.Substring(6, 2)));
        }
        static string[] TimeSplitter(string time)
        {
            return time.Split(':');
        }
        public static DateTime getTime(string date, string time)
        {
            string[] t = TimeSplitter(time);
            return new DateTime(Convert.ToInt16(date.Substring(0, 4)), Convert.ToInt16(date.Substring(4, 2)), Convert.ToInt16(date.Substring(6, 2)), Convert.ToInt16(t[0]), Convert.ToInt16(t[1]), (int)Convert.ToDouble(t[2]));
        }
        public static double getAverage(ArrayList list)
        {
            double total = 0;
            for (int i = 0; i < list.Count; i++)
            {
                total += Convert.ToDouble(list[i]);
            }
            return total / list.Count;
        }
        public static double getMaximum(ArrayList list)
        {
            double max = Convert.ToDouble(list[0]);
            for (int i = 0; i < list.Count; i++)
            {
                if (max < Convert.ToDouble(list[i]))
                {
                    max = Convert.ToDouble(list[i]);
                }
            }
            return max;
        }
        public static double getMinimum(ArrayList list)
        {
            double min = Convert.ToDouble(list[0]);
            for (int i = 0; i > list.Count; i++)
            {
                if (min < Convert.ToDouble(list[i]))
                {
                    min = Convert.ToDouble(list[i]);
                }
            }
            return min;
        }
        public static double GetTotalDistance(ArrayList speedList)
        {
            double dist = 0;

            for (int i = 0; i < speedList.Count; i++)
            {
                double spd = (Convert.ToDouble(speedList[i]) / 10) / 3600;
                dist += spd;
            }
            return Math.Round(dist, 3);
        }
    }
}
