﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polarcycle
{
    class function
    {
        // function fro advance matrix
        public double calcFTP(List<double> powrList)//Functional Threshold Power
        {
            double ftp = Math.Round((powrList.Average() * 0.95), 2);
            return ftp;
        }
        public double calcIF(double np, double ftp)//intensity Factor
        {
            double If = np / ftp;
            return If;
        }
        public double calcNP(List<double> powrList)
        {
            List<double> rollingAvg = new List<double>();
            double avg, np;

            for (int i = 0; i < powrList.Count; i++)
            {
                double tot = 0;
                for (int j = i; j < i + 30; j++)
                {
                    if (j < powrList.Count)
                    {
                        double pwr = powrList[j];
                        tot += pwr;
                    }

                }
                rollingAvg.Add(Math.Pow((tot / 30), 4));
            }
            avg = rollingAvg.Average();
            np = Math.Sqrt(Math.Sqrt(avg));

            return np;

        }
        public double calcTSS(double np, double If, double ftp, string length)//Training stress score calculation
        {
            double tss = 0;
            string[] timeLength = length.Split(':');

            int totSec = (Convert.ToInt32(timeLength[0]) * 60 * 60) + (Convert.ToInt32(timeLength[1]) * 60) + (Convert.ToInt32((timeLength[2]).Split('.')[0]));
            tss = ((totSec * np * If) / (ftp * 3600)) * 100;
            return tss;

        }
    }

}

